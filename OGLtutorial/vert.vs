#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

uniform float offset;
out vec3 ourPos;
out vec3 ourColor;

void main()
{
	vec3 rPos;
	rPos.x = aPos.x + offset;
	rPos.y = -aPos.y;
	rPos.z = aPos.z;

    gl_Position = vec4(rPos, 1.0);
	ourPos = rPos;
    ourColor = aColor;
}