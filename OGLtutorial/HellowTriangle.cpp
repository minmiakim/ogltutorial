﻿#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);

// Vertex Shader
const char *vertexShaderSource = "#version 330 core\n"		//A declaration of its version
"layout (location = 0) in vec3 aPos;\n"						//Declared the input vertex attributes(=position)
"void main()\n"
"{\n"
"   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"		//gl_Position — contains the position of the current vertex
"}\0";
// Fragment Shader
// FragColor = Only one output variable is required which is the final color output 
const char *fragmentShaderSource = "#version 330 core\n"
"out vec4 FragColor;\n"
"void main()\n"
"{\n"
"   FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"	//Orange
"}\n\0";

const char *fragmentShaderSource1 = "#version 330 core\n"
"out vec4 FragColor;\n"
"void main()\n"
"{\n"
"   FragColor = vec4(1.0f, 1.0f, 0.0f, 1.0f);\n"	// Yellow
"}\n\0";

int main()
{
	// glfw: initialize and configure
	// ------------------------------
	glfwInit();														// Initialize GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);					// Option and Option value
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);					// Specify the version should be used
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);	// Specify that we want to use the core-profile

#ifdef __APPLE__
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

	// glfw window creation
	// --------------------
	GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL); // Context Return value is the obj of GLFWwindow
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);			// make the context of this window the main context on the current thread
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);	//Assign CB function

	// glad: load all OpenGL function pointers
	// ---------------------------------------
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))		//GLAD manages function pointers for OpenGL so we want to initialize GLAD 
	{																//before we call any OpenGL function
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	// Create and compile our shader program
	// glCreateShader (Param 1 : the shader type)
	// glShaderSource (param 1 : the shader object, p2 : the number of strings, 
	//						p3 : the actual source code of vs, p4 :  null)
	//				
	//
	//----------------------------------------------------------------------------------
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);
	// check for shader compile errors
	int  success;
	char infoLog[512];
	// Check if compilation was successful
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		// Print the error message
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	// fragment shader
	unsigned int fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);
	// check for shader compile errors
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	// link shaders : 
	// ShaderProgram = the final linked version of several shaders combined.
	// Compile shaders -> link shaders = a shader program-> activate the shader program when rendering object
	int shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);	// Attach each shader
	glAttachShader(shaderProgram, fragmentShader);	// 
	glLinkProgram(shaderProgram);					// Link them

	// check for linking errors
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}

	//Q3. 
	unsigned int fragmentShader1;
	fragmentShader1 = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader1, 1, &fragmentShaderSource1, NULL);
	glCompileShader(fragmentShader1);


	int shaderProgram1 = glCreateProgram();
	glAttachShader(shaderProgram1, vertexShader);
	glAttachShader(shaderProgram1, fragmentShader1);	// 
	glLinkProgram(shaderProgram1);					// Link them




// Eliminate the shader object not to be used.
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	//----------------------------------------------------------------------------------

	/*tell OpenGL the size of the rendering window so OpenGL knows
	how we want to display the data and coordinates with respect to the window.*/
	//glViewport(0, 0, 800, 600);			//left corner, width, height	

	// Save the vertex data at GPU
	// Set up vertex data (and buffer(s)) and configure vertex attributes
	//---------------------------------------
	// normalized device coordinates

	///*Triangle*/
	//float vertices[] = {
	//-0.5f, -0.5f, 0.0f,
	// 0.5f, -0.5f, 0.0f,
	// 0.0f,  0.5f, 0.0f
	//};

	float firstTriangle[] = {
		-0.9f, -0.5f, 0.0f,  // left 
		-0.0f, -0.5f, 0.0f,  // right
		-0.45f, 0.5f, 0.0f,  // top 
	};
	float secondTriangle[] = {
		0.0f, -0.5f, 0.0f,  // left
		0.9f, -0.5f, 0.0f,  // right
		0.45f, 0.5f, 0.0f   // top 
	};

	///*Rectangular*/
	//float vertices[] = {
	//	 0.5f,  0.5f, 0.0f,  // top right
	//	 0.5f, -0.5f, 0.0f,  // bottom right
	//	-0.5f, -0.5f, 0.0f,  // bottom left
	//	-0.5f,  0.5f, 0.0f   // top left 
	//};

	// For Element Buffer Object EBO
	// indexed drawing : store only the unique vertices and then specify the order by indices at which we want to draw 
	unsigned int indices[] = {  // note that we start from 0!
		0, 1, 3,   // first triangle
		1, 2, 3    // second triangle
	};

	// NO USE VAO
	//--------------------------------------------------------------------------------
	//unsigned int VBO;		// 
	//glGenBuffers(1, &VBO);	// Generate buffer ID
	//
	//// 0. Bind with buffer ID and the buffer we call
	//glBindBuffer(GL_ARRAY_BUFFER, VBO);	// VBO buffer type = GL_ARRAY_BUFFER 

	//// 1. Copy the vertices array data in the binded buffer for OGL to use
	//// param 1 : Buffer type
	//// param 2 : the data size (bytes)
	//// param 3 : the actual data to send 
	//// param 4 : the way to manage the given data at GPU
	////			 GL_STATIC_DRAW : the data will most likely not change at all or very rarely.
	////			 GL_DYNAMIC_DRAW: the data is likely to change a lot.
	////			 GL_STREAM_DRAW : the data will change every time it is drawn.
	//glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	//// 2. glVertexAttribPointer(p1, p2, p3, p4, p5, p6) : How it should interpret the vertex data (per vertex attribute)
	//// : Read the data from the VBO currently bound to GL_ARRAY_BUFFER
	//// p1 : Which vertex attribute(pos, color, normal...) to configure (in code(vs), 0 = position)
	//// p2 : the size of the vertex attribute
	//// p3 : the type of the data
	//// p4 : True = Do normalize the data, False = Do not normalize the data
	//// p5 : stride that tells us the space between consecutive vertex attributes
	//// p6 : Where this vertex attribute (the position) data begins in the buffer
	//glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	//glEnableVertexAttribArray(0);

	//// 3. Use the shader program we mad to render an object
	//glUseProgram(shaderProgram);
	//// 4. Now draw the object 
	////someOpenGLFunctionThatDrawsOurTriangle();
	//--------------------------------------------------------------------------------


	/* Vertex array object
		: Stores our vertex attribute configuration and which VBO to use.
	1. Calls to glEnableVertexAttribArray or glDisableVertexAttribArray.
	2. Vertex attribute configurations via glVertexAttribPointer.
	3. Vertex buffer objects associated with vertex attributes by calls to glVertexAttribPointer
	*/
	unsigned int VBO;		// 
	glGenBuffers(1, &VBO);	// Generate buffer ID
	unsigned int VAO;
	glGenVertexArrays(1, &VAO);
	unsigned int EBO;
	glGenBuffers(1, &EBO);

	// 1. bind Vertex Array Object
	glBindVertexArray(VAO);
	// 2. copy our vertices array in a buffer for OpenGL to use
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(firstTriangle), firstTriangle, GL_STATIC_DRAW);

	// 2.1 
	// glBindBuffer : bind the EBO and copy the indices into the buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	// 3. then set our vertex attributes pointers
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);


	// 2-1. note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// remember: do NOT unbind the EBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the EBO bound.
	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// 1-1. You can unbind the VAO afterwards so other VAO calls won't accidentally modify this VAO, but this rarely happens. Modifying other
	// VAOs requires a call to glBindVertexArray anyways so we generally don't unbind VAOs (nor VBOs) when it's not directly necessary.
	glBindVertexArray(0);


	// Q2
	//--------------------------------------------------
	unsigned int VBO1;		// 
	glGenBuffers(1, &VBO1);	// Generate buffer ID
	unsigned int VAO1;
	glGenVertexArrays(1, &VAO1);

	glBindVertexArray(VAO1);
	glBindBuffer(GL_ARRAY_BUFFER, VBO1);
	glBufferData(GL_ARRAY_BUFFER, sizeof(secondTriangle), secondTriangle, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	// Render loop
	while (!glfwWindowShouldClose(window))
	{
		// input
		// -----
		processInput(window);

		// render
		// ------
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);	//State-setting function
		glClear(GL_COLOR_BUFFER_BIT);			//State-using function
												//can use depth buffer, stencil buffer

		// ..:: Drawing code (in render loop) :: ..
		// 4. draw the object
		glUseProgram(shaderProgram);
		glBindVertexArray(VAO); // seeing as we only have a single VAO there's no need to bind it every time, but we'll do so to keep things a bit more organized
		// glDrawArrays (p1, p2, p3)
		// p1 : the type of OpenGL primitive we want to draw
		// p2 : Set the starting index of the vertex array
		// p3 : how many vertices we want to draw
		// Draw Triangle by vertex array
		glDrawArrays(GL_TRIANGLES, 0, 3);

		//Q3
		glUseProgram(shaderProgram1);
		glBindVertexArray(VAO1);
		glDrawArrays(GL_TRIANGLES, 0, 3);



		// EBO) Draw triangle by indices buffer
		//--------------------------------------------------------------------------------
		// glDrawElements(p1, p2, p3, p4)
		// p1 : mode
		// p2 : the number of indices we want to draw
		// p3 : the type of index
		// p4 : an offset in the EBO
		//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
		glBindVertexArray(0); // 1-1. no need to unbind it every time 

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		glfwSwapBuffers(window);		//Swap a Color buffer (Use Double buffer)
		glfwPollEvents();				//Check if an event occur
	}

	glfwTerminate();					//free the resources
	return 0;

}

// Callback function : when the window size is changed
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

//Receive the input of Window
void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}